let collection = [];

// Write the queue functions below.

const print = () =>{
    return collection
}

const enqueue = (element) =>{
    collection[collection.length]=element;
    return collection;
}

const dequeue = () =>{
    for (let i = 0; i < collection.length; i++){
        collection[i] = collection[i+1];
    }
    collection.length = collection.length - 1;
    return collection;
}

const front = () =>{
    return collection[0];
}

const size = () =>{
    return collection.length;
}

let isEmpty = () =>{
    return collection.length === 0;
}


module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};
